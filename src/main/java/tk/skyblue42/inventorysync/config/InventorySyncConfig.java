package tk.skyblue42.inventorysync.config;

import net.minecraftforge.common.ForgeConfigSpec;

public final class InventorySyncConfig 
{
    public static final ForgeConfigSpec.Builder BUILDER = new ForgeConfigSpec.Builder();
    public static final ForgeConfigSpec SPEC;

    // Config fields
    public static final ForgeConfigSpec.ConfigValue<String> redis_host;
    public static final ForgeConfigSpec.ConfigValue<Integer> redis_port;
    public static final ForgeConfigSpec.ConfigValue<String> redis_user;
    public static final ForgeConfigSpec.ConfigValue<String> redis_password;
    public static final ForgeConfigSpec.ConfigValue<Integer> redis_database;
    
    public static final ForgeConfigSpec.ConfigValue<String> server_id;

    static {
        BUILDER.push("Redis config");

        redis_host = BUILDER.comment("Hostname or IP address for the redis server").define("redis_host", "localhost");
        redis_port = BUILDER.comment("Port of the redis server").define("redis_port", 6379);
        redis_user = BUILDER.comment("Username to authenticate with redis server").define("redis_user", "default");
        redis_password = BUILDER.comment("Password of the redis user").define("redis_password", "");
        redis_database = BUILDER.comment("The database number").define("redis_database", 0);

        BUILDER.pop();

        BUILDER.push("Cross-server chat config");

        server_id = BUILDER.comment("A unique ID of the server. Can be anything but recommended to input a short string.").define("server_id", "main");

        BUILDER.pop();

        // Build the spec
        SPEC = BUILDER.build();
    }
}
