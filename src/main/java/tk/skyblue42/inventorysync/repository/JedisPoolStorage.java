package tk.skyblue42.inventorysync.repository;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.config.InventorySyncConfig;

public class JedisPoolStorage 
{
    private static JedisPoolStorage instance;

    private JedisPool pool;

    private JedisPoolStorage()
    {
        pool = new JedisPool(
            new JedisPoolConfig(), 
            InventorySyncConfig.redis_host.get(), 
            InventorySyncConfig.redis_port.get(), 
            10000, 
            InventorySyncConfig.redis_user.get(),
            InventorySyncConfig.redis_password.get(),
            InventorySyncConfig.redis_database.get()
        );
    }

    public static JedisPoolStorage getInstance()
    {
        if (instance == null) {
            instance = new JedisPoolStorage();
        }

        return instance;
    }

    public JedisPool getJedisPool()
    {
        return pool;
    }
}
