package tk.skyblue42.inventorysync.repository;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Stack;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.chat.ChatMessage;
import tk.skyblue42.inventorysync.config.InventorySyncConfig;

public class ChatRepository 
{
    private static ChatRepository instance;

    private static final JedisPool POOL = JedisPoolStorage.getInstance().getJedisPool();

    public static LocalDateTime lastCheckedDateTime = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.ofHours(0));

    public static final String CHAT_KEY = "chat";
    public static final String SERVER_COUNT_KEY = "num_servers";
    
    public static final int BATCH_SIZE = 5;

    private ChatRepository()
    {

    }

    public void appendMessage(ChatMessage message)
    {
        try (Jedis jedis = POOL.getResource()) {
            jedis.lpush(CHAT_KEY, message.toSnbt());
        }
    }

    public Stack<ChatMessage> getNewMessages()
    {
        Stack<ChatMessage> newMessages = new Stack<ChatMessage>();

        int i = 0;

        while (true) {
            try (Jedis jedis = POOL.getResource()) {
                // InventorySync.LOGGER.debug("Getting new messages, i = {}", i);

                List<String> messages;
                try {
                    messages = jedis.lrange(CHAT_KEY, i, BATCH_SIZE);
                } catch (Throwable e) {
                    InventorySync.LOGGER.error("Failed to get messages, {}", e);
                    return newMessages;
                }

                // InventorySync.LOGGER.debug("Got {} new messages", messages.size());
                
                if (messages.isEmpty()) {
                    break;
                }

                for (String messageString : messages) {
                    try {
                        ChatMessage message = ChatMessage.fromSnbt(messageString);

                        // Message is already sent
                        if (message.getTime().isBefore(lastCheckedDateTime)) {
                            // InventorySync.LOGGER.debug("Message is already processed, breaking");
                            break;
                        }

                        if (message.getServerId().equals(InventorySyncConfig.server_id.get())) {
                            // InventorySync.LOGGER.debug("Message is from our own, not sending");
                            continue;
                        }

                        newMessages.push(message);
                    } catch (Throwable e) {
                        InventorySync.LOGGER.error("Cannot deserialize message", e);
                    }
                }
            }

            i += BATCH_SIZE;
        }

        lastCheckedDateTime = LocalDateTime.now();

        return newMessages;
    }

    public void incrementServerCount()
    {
        try (Jedis jedis = POOL.getResource()) {
            jedis.incr(SERVER_COUNT_KEY);
        }
    }

    /**
     * @return true if the last server have disconnected
     */
    public boolean decrementServerCount()
    {
        long result;

        try (Jedis jedis = POOL.getResource()) {
            result = jedis.decr(SERVER_COUNT_KEY);
            InventorySync.LOGGER.debug("Decremented server count, {} servers still online", result);
        }

        return result == 0;
    }

    public void deleteChat()
    {
        try (Jedis jedis = POOL.getResource()) {
            jedis.del(CHAT_KEY);
        }
    }

    public static ChatRepository getInstance()
    {
        if (instance == null) {
            instance = new ChatRepository();
        }

        return instance;
    }
}
