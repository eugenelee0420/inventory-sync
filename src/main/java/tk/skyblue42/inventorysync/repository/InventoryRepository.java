package tk.skyblue42.inventorysync.repository;

import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.nbt.Tag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import tk.skyblue42.inventorysync.InventorySync;

public class InventoryRepository 
{
    private static final JedisPool POOL = JedisPoolStorage.getInstance().getJedisPool();

    private static InventoryRepository instance;

    private InventoryRepository()
    {

    }

    public static InventoryRepository getInstance()
    {
        if (instance == null) {
            instance = new InventoryRepository();
        }

        return instance;
    }

    public void saveInventory(Player player)
    {
        this.saveInventory(player, true);
    }

    public void saveInventory(Player player, boolean async)
    {
        // Serialize player's inventory
        String inventoryString = serializeInventory(player);

        // Get player's UUID
        String uuid = player.getUUID().toString();

        // Save to redis
        if (!async) {
            try (Jedis jedis = POOL.getResource()) {
                InventorySync.LOGGER.debug("Setting key: {} value: {}", uuid, inventoryString);
                jedis.set(uuid, inventoryString);
            }
        } else {
            (new Thread() {
                @Override
                public void run() {
                    try (Jedis jedis = POOL.getResource()) {
                        InventorySync.LOGGER.debug("Saving inventory asynchronously");
                        InventorySync.LOGGER.debug("Setting key: {} value: {}", uuid, inventoryString);
                        jedis.set(uuid, inventoryString);
                    }
                }
            }).start();
        }
    }

    public void loadInventory(Player player) throws CommandSyntaxException
    {
        String uuid = player.getUUID().toString();

        String inventoryString;

        try (Jedis jedis = POOL.getResource()) {
            inventoryString = jedis.get(uuid);
            InventorySync.LOGGER.debug("Got key: {} value: {}", uuid, inventoryString);
        }

        if (inventoryString == null || inventoryString.isEmpty() || inventoryString.equals("nil")) {
            return;
        }

        CompoundTag tag = NbtUtils.snbtToStructure(inventoryString);

        ListTag inventoryListTag = tag.getList("Inventory", Tag.TAG_COMPOUND);
        ListTag enderChestListTag = tag.getList("EnderChest", Tag.TAG_COMPOUND);

        Inventory inventory = new Inventory(player);

        inventory.load(inventoryListTag);
        player.getInventory().replaceWith(inventory);

        player.getEnderChestInventory().fromTag(enderChestListTag);
    }

    private String serializeInventory(Player player)
    {
        ListTag inventoryListTag = player.getInventory().save(new ListTag());
        ListTag enderChestListTag = player.getEnderChestInventory().createTag();

        CompoundTag tag = new CompoundTag();
        tag.put("Inventory", inventoryListTag);
        tag.put("EnderChest", enderChestListTag);

        return NbtUtils.structureToSnbt(tag);
    }
}
