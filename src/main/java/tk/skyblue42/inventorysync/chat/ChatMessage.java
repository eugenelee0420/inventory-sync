package tk.skyblue42.inventorysync.chat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextColor;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.ServerChatEvent;
import tk.skyblue42.inventorysync.config.InventorySyncConfig;

public class ChatMessage 
{
    private String message;
    private String username;
    private UUID uuid;

    private String serverId;

    private LocalDateTime time;

    public ChatMessage(Player player, String message)
    {
        this.username = player.getName().getContents();
        this.uuid = player.getUUID();
        this.message = message;
        this.serverId = InventorySyncConfig.server_id.get();
        this.time = LocalDateTime.now();
    }    

    public ChatMessage(ServerChatEvent event)
    {
        this(event.getPlayer(), event.getMessage());
    }

    private ChatMessage()
    {

    }

    public LocalDateTime getTime()
    {
        return time;
    }

    public String getServerId()
    {
        return serverId;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public Component toComponent()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String timeString = formatter.format(this.time);

        // Compose the text message
        TextComponent serverIdComponent = new TextComponent(String.format("(%s) ", this.serverId));

        TextComponent timeComponent = new TextComponent(String.format("[%s] ", timeString));
        timeComponent.setStyle(timeComponent.getStyle().withColor(TextColor.fromRgb(0x00ffff)));

        TextComponent usernameComponent = new TextComponent(String.format("<%s> ", this.username));
        usernameComponent.setStyle(usernameComponent.getStyle().withColor(TextColor.fromRgb(0x39ff14)));

        TextComponent messageComponent = new TextComponent(this.message);
        messageComponent.setStyle(messageComponent.getStyle().withColor(TextColor.fromRgb(0xf5f5f5)));

        serverIdComponent
            .append(timeComponent)
            .append(usernameComponent)
            .append(messageComponent)
        ;

        return serverIdComponent;
    }

    public String toSnbt()
    {
        CompoundTag tag = new CompoundTag();

        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

        CompoundTag playerTag = new CompoundTag();

        playerTag.putString("Username", this.username);
        playerTag.putString("UUID", this.uuid.toString());

        tag.putString("ServerId", this.serverId);
        tag.put("Player", playerTag);
        tag.putString("Time", formatter.format(this.time));
        tag.putString("Message", this.message);

        return NbtUtils.structureToSnbt(tag);
    }

    public static ChatMessage fromSnbt(String snbt) throws CommandSyntaxException
    {
        return fromCompoundTag(NbtUtils.snbtToStructure(snbt));
    }

    public static ChatMessage fromCompoundTag(CompoundTag tag)
    {
        ChatMessage message = new ChatMessage();

        CompoundTag playerTag = tag.getCompound("Player");

        message.username = playerTag.getString("Username");
        message.uuid = UUID.fromString(playerTag.getString("UUID"));

        message.message = tag.getString("Message");
        message.time = LocalDateTime.parse(tag.getString("Time"), DateTimeFormatter.ISO_DATE_TIME);
        message.serverId = tag.getString("ServerId");

        return message;
    }
}
