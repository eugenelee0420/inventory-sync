package tk.skyblue42.inventorysync.event;


import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.repository.InventoryRepository;

@EventBusSubscriber(modid = InventorySync.MOD_ID, bus = Bus.FORGE, value = Dist.DEDICATED_SERVER)
public class PlayerLoadEventHandler 
{
    // @SubscribeEvent
    // public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event)
    // {
    //     LOGGER.debug("Player login");
    // }

    @SubscribeEvent
    public static void onPlayerLoad(PlayerEvent.LoadFromFile event)
    {
        InventorySync.LOGGER.info("Load from file");

        Player player = event.getPlayer();
        
        // Create a new inventory
        // Inventory inventory = new Inventory(player);

        // inventory.add(new ItemStack(Items.DIRT, 42));
        // player.getInventory().replaceWith(inventory);

        // LOGGER.info("Player has class {}", event.getPlayer().getClass());

        // String uuid = event.getPlayerUUID();
        
        try {
            InventoryRepository.getInstance().loadInventory(player);
        } catch (CommandSyntaxException e) {
            InventorySync.LOGGER.error("Unable to load inventory from repository", e);
        }
    }

    // @SubscribeEvent
    // public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event)
    // {
    //     LOGGER.info("LOGIN");
    //     LOGGER.info("Player has class {}", event.getPlayer().getClass());
    // }
}
