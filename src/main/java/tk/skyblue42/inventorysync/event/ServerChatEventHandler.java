package tk.skyblue42.inventorysync.event;

import net.minecraft.network.chat.ChatType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.chat.ChatMessage;
import tk.skyblue42.inventorysync.repository.ChatRepository;

@EventBusSubscriber(modid = InventorySync.MOD_ID, bus = Bus.FORGE, value = Dist.DEDICATED_SERVER)
public class ServerChatEventHandler 
{
    @SubscribeEvent
    public static void onServerChat(ServerChatEvent event)
    {
        InventorySync.LOGGER.debug("ServerChatEvent: <{}>: {}", event.getUsername(), event.getMessage());
        
        // Cancel the event
        event.setCanceled(true);

        // Create a message from the event
        ChatMessage message = new ChatMessage(event);

        // Send the message to all players on this server
        event.getPlayer().server.getPlayerList().broadcastMessage(message.toComponent(), ChatType.CHAT, event.getPlayer().getUUID());

        // Add the message to database
        ChatRepository.getInstance().appendMessage(message);
    }
}
