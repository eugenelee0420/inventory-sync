package tk.skyblue42.inventorysync.event;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.repository.InventoryRepository;

@EventBusSubscriber(modid = InventorySync.MOD_ID, bus = Bus.FORGE, value = Dist.DEDICATED_SERVER)
public class PlayerSaveEventHandler 
{
    @SubscribeEvent
    public static void onPlayerSave(PlayerEvent.SaveToFile event)
    {
        InventorySync.LOGGER.debug("Save to file");

        InventoryRepository.getInstance().saveInventory(event.getPlayer());
    }    
}
