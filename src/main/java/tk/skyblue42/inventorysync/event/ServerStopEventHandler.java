package tk.skyblue42.inventorysync.event;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.server.ServerStoppedEvent;
import net.minecraftforge.event.server.ServerStoppingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.repository.ChatRepository;

@EventBusSubscriber(modid = InventorySync.MOD_ID, bus = Bus.FORGE, value = Dist.DEDICATED_SERVER)
public class ServerStopEventHandler 
{
    @SubscribeEvent
    public static void onServerStop(ServerStoppedEvent event)
    {
        InventorySync.LOGGER.debug("Server stop");
        if (ChatRepository.getInstance().decrementServerCount()) {
            InventorySync.LOGGER.info("Last server to shut down, deleting chat records");
            ChatRepository.getInstance().deleteChat();
        }
    }
}
