package tk.skyblue42.inventorysync.event;

import java.time.LocalDateTime;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.server.ServerAboutToStartEvent;
import net.minecraftforge.event.server.ServerStartedEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.repository.ChatRepository;

@EventBusSubscriber(modid = InventorySync.MOD_ID, bus = Bus.FORGE, value = Dist.DEDICATED_SERVER)
public class ServerStartEventHandler 
{
    // Before the server starts
    @SubscribeEvent
    public static void onAboutToStart(ServerAboutToStartEvent event)
    {
        InventorySync.LOGGER.debug("Before server start");

        // Set chat handler last check time
        // To prevent dumping all messages to chat
        // Run before server start to prevent world tick event firing
        ChatRepository.lastCheckedDateTime = LocalDateTime.now();

        ChatRepository.getInstance().incrementServerCount();
    }
}
