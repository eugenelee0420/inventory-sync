package tk.skyblue42.inventorysync.event;

import java.util.Stack;

import net.minecraft.network.chat.ChatType;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import tk.skyblue42.inventorysync.InventorySync;
import tk.skyblue42.inventorysync.chat.ChatMessage;
import tk.skyblue42.inventorysync.repository.ChatRepository;
import tk.skyblue42.inventorysync.repository.InventoryRepository;

@EventBusSubscriber(modid = InventorySync.MOD_ID, bus = Bus.FORGE, value = Dist.DEDICATED_SERVER)
public class WorldTickEventHandler 
{
    @SubscribeEvent
    public static void onWorldTick(WorldTickEvent event)
    {
        if (
            event.phase.equals(Phase.START)
            && event.side.equals(LogicalSide.SERVER)
            && event.type.equals(TickEvent.Type.WORLD)
            && event.world.dimension().equals(Level.OVERWORLD)
        ) {
            // Every 2 seconds
            if (event.world.getGameTime() % 40 == 0) {
                broadcastNewChatMessage(event);
            }
        }
    }

    private static void broadcastNewChatMessage(WorldTickEvent event)
    {
        Stack<ChatMessage> newMessages = ChatRepository.getInstance().getNewMessages();
        // InventorySync.LOGGER.debug("Checking for new messages");

        while (!newMessages.empty()) {
            ChatMessage newMessage = newMessages.pop();
            event.world.getServer().getPlayerList().broadcastMessage(newMessage.toComponent(), ChatType.CHAT, newMessage.getUuid());
        }
    }
}
