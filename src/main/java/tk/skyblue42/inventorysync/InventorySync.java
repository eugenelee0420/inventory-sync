package tk.skyblue42.inventorysync;

import com.mojang.logging.LogUtils;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import tk.skyblue42.inventorysync.config.InventorySyncConfig;

import org.slf4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("inventorysync")
public class InventorySync
{
    // Directly reference a slf4j logger
    public static final Logger LOGGER = LogUtils.getLogger();

    public static final String MOD_ID = "inventorysync";

    public InventorySync()
    {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);

        // Register event handlers, only when on dedicated servers
        // DistExecutor.unsafeRunWhenOn(Dist.DEDICATED_SERVER, () -> () -> {
        //     MinecraftForge.EVENT_BUS.register(new PlayerLoadEventHandler());
        //     MinecraftForge.EVENT_BUS.register(new PlayerSaveEventHandler());
        // });
        
        // Register config
        ModLoadingContext.get().registerConfig(net.minecraftforge.fml.config.ModConfig.Type.SERVER, InventorySyncConfig.SPEC, "inventory-sync.toml");
    }

}
